﻿using System.ComponentModel.DataAnnotations;

namespace ThirdExamDotNet.Models
{
    public class Profession
    {
        [Key]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "El nombre es obligatorio")]
        [Display(Name = "Nombre")]
        public String Name { get; set; }

        public virtual ICollection<Instructor>? Instructors { get; set; }
    }
}
