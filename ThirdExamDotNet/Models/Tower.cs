﻿using System.ComponentModel.DataAnnotations;

namespace ThirdExamDotNet.Models
{
    public class Tower
    {
        [Key]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "La zona es obligatoria")]
        [Display(Name = "Zona")]
        public String Zone { get; set; }

        public virtual ICollection<Classroom>? Classrooms { get; set; }
    }
}
