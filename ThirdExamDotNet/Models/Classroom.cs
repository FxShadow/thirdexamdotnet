﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ThirdExamDotNet.Models
{
    public class Classroom
    {
        [Key]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "El codigo es obligatorio")]
        [Display(Name = "Codigo")]
        public String Code { get; set; }

        [Required(ErrorMessage = "El piso es obligatorio")]
        [Display(Name = "Piso")]
        public int Floor { get; set; }

        [Required(ErrorMessage = "La torre es obligatoria")]
        [Display(Name = "Torre")]
        public Guid TowerId { get; set; }
        [ForeignKey("TowerId")]

        public virtual Tower? Tower { get; set; }

        [Required(ErrorMessage = "El instructor es obligatorio")]
        [Display(Name = "Instructor")]
        public Guid InstructorId { get; set; }        
        [ForeignKey("InstructorId")]

        public virtual Instructor? Instructor { get; set; }

        public virtual ICollection<Student>? Students { get; set; }
    }
}
