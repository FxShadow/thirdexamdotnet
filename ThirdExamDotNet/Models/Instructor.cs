﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ThirdExamDotNet.Models
{
    public class Instructor
    {
        [Key]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "El documento es obligatorio")]
        [Display(Name = "Documento")]
        public int Document { get; set; }

        [Required(ErrorMessage = "El nombre es obligatorio")]
        [Display(Name = "Nombre Completo")]
        public String FullName { get; set; }

        [Required(ErrorMessage = "El correo es obligatorio")]
        [Display(Name = "Correo Electronico")]
        public String Email { get; set; }

        [Required(ErrorMessage = "El telefono es obligatorio")]
        [Display(Name = "Telefono")]
        public int Phone { get; set; }

        [Required(ErrorMessage = "La profesion es obligatoria")]
        [Display(Name = "Profesion")]
        public Guid ProfessionId { get; set; }
        [ForeignKey("ProfessionId")]

        public virtual Profession? Profession { get; set; }

        public virtual ICollection<Classroom>? Classrooms { get; set; }


    }
}
