using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using ThirdExamDotNet.Context;

namespace ThirdExamDotNet.Models
{
    public class StudyProgram
    {
        [Key]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "El nombre es obligatorio")]
        [Display(Name = "Nombre")]
        public String Name { get; set; }
        public virtual ICollection<Student>? Students { get; set; }
    }
}

