﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ThirdExamDotNet.Models
{
    public class Student
    {
        [Key]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "El documento es obligatorio")]
        [Display(Name = "Documento")]
        public int Document { get; set; }

        [Required(ErrorMessage = "El nombre es obligatorio")]
        [Display(Name = "Nombre Completo")]
        public String FullName { get; set; }

        [Required(ErrorMessage = "El correo es obligatorio")]
        [Display(Name = "Correo Electronico")]
        public String Email { get; set; }

        [Required(ErrorMessage = "El telefono es obligatorio")]
        [Display(Name = "Telefono")]
        public int Phone { get; set; }

        [Required(ErrorMessage = "La direccion es obligatoria")]
        [Display(Name = "Direccion")]
        public String Address { get; set; }

        [Required(ErrorMessage = "El programa de formacion es obligatorio")]
        [Display(Name = "Programa de Formacion")]
        public Guid StudyProgramId { get; set; }
        [ForeignKey("StudyProgramId")]

        public virtual StudyProgram? StudyProgram { get; set; }

        [Required(ErrorMessage = "El ambiente es obligatorio")]
        [Display(Name = "Ambiente")]
        public Guid ClassroomId { get; set; }
        [ForeignKey("ClassroomId")]

        public virtual Classroom? Classroom { get; set; }
    }
}

   

