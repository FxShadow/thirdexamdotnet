﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ThirdExamDotNet.Context;
using ThirdExamDotNet.Models;

namespace ThirdExamDotNet.Controllers
{
    public class StudyProgramsController : Controller
    {
        private readonly ContextDb _context;

        public StudyProgramsController(ContextDb context)
        {
            _context = context;
        }

        // GET: StudyPrograms
        public async Task<IActionResult> Index()
        {
              return _context.StudyPrograms != null ? 
                          View(await _context.StudyPrograms.ToListAsync()) :
                          Problem("Entity set 'ContextDb.StudyPrograms'  is null.");
        }

        // GET: StudyPrograms/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null || _context.StudyPrograms == null)
            {
                return NotFound();
            }

            var studyProgram = await _context.StudyPrograms
                .FirstOrDefaultAsync(m => m.Id == id);
            if (studyProgram == null)
            {
                return NotFound();
            }

            return View(studyProgram);
        }

        // GET: StudyPrograms/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: StudyPrograms/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name")] StudyProgram studyProgram)
        {
            if (ModelState.IsValid)
            {
                studyProgram.Id = Guid.NewGuid();
                _context.Add(studyProgram);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(studyProgram);
        }

        // GET: StudyPrograms/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null || _context.StudyPrograms == null)
            {
                return NotFound();
            }

            var studyProgram = await _context.StudyPrograms.FindAsync(id);
            if (studyProgram == null)
            {
                return NotFound();
            }
            return View(studyProgram);
        }

        // POST: StudyPrograms/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Id,Name")] StudyProgram studyProgram)
        {
            if (id != studyProgram.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(studyProgram);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StudyProgramExists(studyProgram.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(studyProgram);
        }

        // GET: StudyPrograms/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null || _context.StudyPrograms == null)
            {
                return NotFound();
            }

            var studyProgram = await _context.StudyPrograms
                .FirstOrDefaultAsync(m => m.Id == id);
            if (studyProgram == null)
            {
                return NotFound();
            }

            return View(studyProgram);
        }

        // POST: StudyPrograms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            if (_context.StudyPrograms == null)
            {
                return Problem("Entity set 'ContextDb.StudyPrograms'  is null.");
            }
            var studyProgram = await _context.StudyPrograms.FindAsync(id);
            if (studyProgram != null)
            {
                _context.StudyPrograms.Remove(studyProgram);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StudyProgramExists(Guid id)
        {
          return (_context.StudyPrograms?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
