﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ThirdExamDotNet.Context;
using ThirdExamDotNet.Models;

namespace ThirdExamDotNet.Controllers
{
    public class TowersController : Controller
    {
        private readonly ContextDb _context;

        public TowersController(ContextDb context)
        {
            _context = context;
        }

        // GET: Towers
        public async Task<IActionResult> Index()
        {
              return _context.Towers != null ? 
                          View(await _context.Towers.ToListAsync()) :
                          Problem("Entity set 'ContextDb.Towers'  is null.");
        }

        // GET: Towers/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null || _context.Towers == null)
            {
                return NotFound();
            }

            var tower = await _context.Towers
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tower == null)
            {
                return NotFound();
            }

            return View(tower);
        }

        // GET: Towers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Towers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Zone")] Tower tower)
        {
            if (ModelState.IsValid)
            {
                tower.Id = Guid.NewGuid();
                _context.Add(tower);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tower);
        }

        // GET: Towers/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null || _context.Towers == null)
            {
                return NotFound();
            }

            var tower = await _context.Towers.FindAsync(id);
            if (tower == null)
            {
                return NotFound();
            }
            return View(tower);
        }

        // POST: Towers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Id,Zone")] Tower tower)
        {
            if (id != tower.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tower);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TowerExists(tower.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tower);
        }

        // GET: Towers/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null || _context.Towers == null)
            {
                return NotFound();
            }

            var tower = await _context.Towers
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tower == null)
            {
                return NotFound();
            }

            return View(tower);
        }

        // POST: Towers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            if (_context.Towers == null)
            {
                return Problem("Entity set 'ContextDb.Towers'  is null.");
            }
            var tower = await _context.Towers.FindAsync(id);
            if (tower != null)
            {
                _context.Towers.Remove(tower);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TowerExists(Guid id)
        {
          return (_context.Towers?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
