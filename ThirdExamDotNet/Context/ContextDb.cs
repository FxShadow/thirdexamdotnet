﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ThirdExamDotNet.Models;

namespace ThirdExamDotNet.Context
{
    public class ContextDb : IdentityDbContext
    {
        public ContextDb(DbContextOptions<ContextDb> options) : base(options) { }

        public DbSet<Tower> Towers { get; set; }

        public DbSet<Profession> Professions { get; set; }

        public DbSet<Instructor> Instructors { get; set; }

        public DbSet<Classroom> Classrooms { get; set; }

        public DbSet <StudyProgram> StudyPrograms { get; set; }

        public DbSet<Student> Students { get; set; }

    }
}
